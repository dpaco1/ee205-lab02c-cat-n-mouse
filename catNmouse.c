///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// author  David Paco <dpaco@hawaii.edu>
/// date    26_1_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main( int argc, char* argv[] ) 
{
   int theMaxValue = atoi( argv[1] );
   if ( theMaxValue >= 1 ) {
      printf( "Cat `n Mouse\n" );

      // This is an example of getting a number from a user
      // Note:  Don't enter unexpected values like 'blob' or 1.5.  
      int aGuess;
      int correct = 0;
      // Generate Random Number
      srand(time(0));
      int answer = (rand() % theMaxValue) + 1;
      // printf("The generated number is: [%d]\n", answer ); // Checks Random Generated Number
      do {
         printf( "OK cat, I'm thinking of a number from 1 to %d. Make a guess: ", theMaxValue );
         scanf( "%d", &aGuess );
      // printf( "The number was [%d]\n", aGuess ); // Checks User Guess
            if( aGuess > theMaxValue )
            {
               printf( "You must enter a number that's <= %d\n", theMaxValue);
            }
            else if( aGuess < 1 )
            {
               printf( "You must enter a number that's >= 1\n");
            }
            else if( aGuess > answer )
            {
               printf( "No cat... the number I'm thinking of is smaller than %d\n", aGuess);
            }
            else if( aGuess < answer )
            {
               printf( "No cat... the number I'm thinking of is larger than %d\n", aGuess);
            }
            else if( aGuess == answer )
            {
               printf( "You got me.\n");
               printf( "    ,-. __ .-,\n");
               printf( "  --;`. '   `.'\n");
               printf( "   / (  ^__^  )\n");
               printf( "  ;   `(_`'_)'  \n");
               printf( " '  ` .`--'_,  ;\n");
               printf( "~~`-..._)))(((.'\n");
               correct = 1;
            }
   } while( correct != 1);
      return 0;
   }
   else {
      printf( "ERROR: Command Line Input %d Not a NUMBER\n", theMaxValue);
      exit (1);
   }
}
